from typing import Optional
from pydantic import BaseModel



class EssayExam(BaseModel):
    question_id: Optional[int] = None
    information_text: str
    level_id: int

class EssayAnswer(BaseModel):
    question_id: int
    content: str
from typing import Optional
from pydantic import BaseModel, EmailStr, Field

class SpeakingConversation(BaseModel):
    thread_id: str
    user_response: str
# models/user.py

from typing import Optional
from pydantic import BaseModel, EmailStr, Field

class UserRegistration(BaseModel):
    username: str
    password: str
    user_email: EmailStr

class UserLogin(BaseModel):
    identifier: Optional[str] = Field(None, description="Can be either username or user_email")
    password: str

class User(BaseModel):
    user_id: int
    username: str
    user_email: EmailStr

class UserSubscription(BaseModel):
    identifier: Optional[str] = Field(None, description="Can be either username or user_email")
    subscription_id: int

class PasswordChangeRequest(BaseModel):
    old_password: str
    new_password: str
    confirm_new_password: str

class ForgotPasswordRequest(BaseModel):
    username_or_email: str

class ResetPasswordRequest(BaseModel):
    token: str
    new_password: str

class GoogleAuthCode(BaseModel):
    code: str  # The authorization code from Google
# models/smart_multiple_choice
from typing import Optional
from pydantic import BaseModel, Field

class QuestionBase(BaseModel):
    id_question: Optional[int] = None
    information_text: str
    question: str
    answer_1: str
    answer_2: str
    answer_3: str
    correct_answer: str
    level_id: int
    question_type: str

class SmartQuestion(QuestionBase):
    pass

class ListeningQuestion(QuestionBase):
    audio_url: str
    
    

class GrammarQuestion(QuestionBase):
    topic: str



class MultipleChoiceAnswerFromUser(BaseModel):
    id_question: int
    user_answer: str
    question_type: str

class MultipleChoiceAnswerFromUserListening(BaseModel):
    id_question: int
    user_answer: str
    
from pydantic import BaseModel, EmailStr

class ContactDetails(BaseModel):
    user_email: EmailStr
    title: str
    message: str

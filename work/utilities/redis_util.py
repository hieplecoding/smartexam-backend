import os
import redis
from functools import lru_cache
from urllib.parse import urlparse  # For parsing the REDIS_URL

@lru_cache()
def get_redis_connection():
    redis_url = os.environ.get("REDIS_URL")
    if not redis_url:
        raise ValueError("Missing REDIS_URL in environment variables.")
    
    # Parse the URL to check if it's secure (rediss://) or not
    parsed_url = urlparse(redis_url)
    
    # Automatically determine SSL usage based on the URL scheme
    if parsed_url.scheme == "rediss":
        # For rediss:// (SSL), we enable SSL
        return redis.Redis.from_url(redis_url, decode_responses=True, ssl_cert_reqs=None)
    else:
        # For redis:// (no SSL), no additional SSL params are necessary
        return redis.Redis.from_url(redis_url, decode_responses=True)


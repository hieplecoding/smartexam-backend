import os
from openai import OpenAI

# Set up the OpenAI client
def get_client():
    api_key = os.getenv("OPENAI_API_KEY")
    client = OpenAI(api_key=api_key)
    return client

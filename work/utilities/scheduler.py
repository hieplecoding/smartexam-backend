from work.db_connect.db_connect import create_connection


async def deactivate_expired_subscriptions():
    conn = await create_connection()
    if conn is not None:
        try:
            # Set the timezone to "Europe/Vienna" for this session
            await conn.execute("SET TIME ZONE 'Europe/Vienna';")

            query = """
            UPDATE user_subscriptions
            SET status = 'inactive'
            WHERE end_date < CURRENT_TIMESTAMP AND status = 'active'
            """
            result = await conn.execute(query)
            print(result)  # This will print the result of the UPDATE operation, e.g., "UPDATE 5"
        except Exception as e:
            print(f"Failed to deactivate subscriptions: {e}")
        finally:
            await conn.close()
    else:
        print("Failed to connect to the database.")

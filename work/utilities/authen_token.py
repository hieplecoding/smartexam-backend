# utilities/auth_token.py
import jwt
from fastapi import Depends, HTTPException, status
from work.utilities.token import verify_access_token
from fastapi.security import OAuth2PasswordBearer

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer "},
    )
    try:
        payload = verify_access_token(token)
        user_id: int = payload.get("sub")
        if user_id is None:
            raise credentials_exception
        return user_id  # return only the username
    except jwt.PyJWTError:
        raise credentials_exception
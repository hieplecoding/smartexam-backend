

from work.repositories.essay.show_essay_question_repo import get_writing_question
from work.utilities.openai_client import get_client


async def process_checking_essay(way_to_check, question_id, content, db):
    try:
        # Retrieve the essay question from the database
        essay_exam = await get_writing_question(question_id, db)
        question_text = essay_exam["information_text"]

        if way_to_check == "grammar_check":
            prompt = (
                "Bạn là một giáo viên dạy tiếng Đức thành thạo tiếng Việt, đang chấm một bài luận do một học sinh nộp. Nhiệm vụ của bạn là kiểm tra kỹ lưỡng bài luận để tìm lỗi ngữ pháp, lỗi chính tả.\n\n"
                f"Chủ đề: {question_text}\n\n"
                f"Bài luận: {content}\n\n"
                "Trong phản hồi của bạn, hãy chỉ ra cụ thể những khu vực cần cải thiện và đề xuất cách sửa lỗi bằng tiếng Việt. Mục tiêu của bạn là giúp học sinh hiểu ra lỗi của mình và cải thiện kỹ năng viết. Vui lòng chỉ đưa ra phản hồi của bạn hoàn toàn bằng tiếng Việt."
            )
        elif way_to_check == "essay_check":
            prompt = (
                "Bạn là một giáo viên dạy tiếng Đức thành thạo tiếng Việt, đang chấm một bài luận do một học sinh nộp. Nhiệm vụ của bạn là đánh giá tổng thể bài luận, bao gồm nội dung, cách thể hiện ý tưởng và tính sáng tạo. Vui lòng phân tích và đưa ra phản hồi mang tính xây dựng bằng tiếng Việt.\n\n"
                f"Chủ đề: {question_text}\n\n"
                f"Bài luận: {content}\n\n"
                "Trong phản hồi của bạn, hãy đề cập đến điểm mạnh cũng như cách thức cải thiện các điểm yếu, với mục đích nâng cao khả năng diễn đạt và sáng tạo của học sinh. Vui lòng chỉ đưa ra phản hồi của bạn hoàn toàn bằng tiếng Việt."
            )
        else:
            return "Invalid way_to_check value", False

        # Sending the prompt to OpenAI API using the Completion endpoint
        client = get_client()
        response = client.chat.completions.create(
            model="gpt-4o",
            messages=[
                {
                    "role": "system",
                    "content": "Bạn là một giáo viên dạy tiếng Đức, thành thạo tiếng Anh và tiếng Việt, và bạn cần phải kiểm tra các bài luận một cách cẩn thận. Vui lòng chỉ đưa ra phản hồi của bạn hoàn toàn bằng tiếng Việt.\n"
                    + prompt,
                },
                {"role": "user", "content": "Antwort: " + content},
            ],
        )

        # Extracting and formatting the response to ensure it's in Vietnamese
        feedback = response.choices[
            0
        ].message  # Assuming this contains the feedback in Vietnamese
        return feedback, True
    except Exception as e:
        print(e)
        return str(e), False

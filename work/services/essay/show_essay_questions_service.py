from typing import List
import os
import json 
from work.utilities.redis_util import get_redis_connection
from work.models.essay import EssayExam
from work.repositories.essay.show_essay_question_repo import show_writing_question_repo


async def process_show_writing_question(level, db) -> List[EssayExam]:
    redis_conn = get_redis_connection()
    cache_key = f"writing_questions:{level}"
    
    # Try to fetch the questions from cache
    cached_questions = redis_conn.get(cache_key)
    if cached_questions:
        # If found in cache, return them after deserializing
        return json.loads(cached_questions)
    
    # If not found in cache, query the database
    question_rows = await show_writing_question_repo(level, db)
    # Ensure question_rows is not None before iterating
    if question_rows is None:
        return []

    questions = []
    for row in question_rows:
        question = EssayExam(
            question_id=row['question_id'],
            information_text=row['information_text'],
            level_id=row['level_id']
        )
        questions.append(question.dict())
    
    # Cache the questions for future requests
    # Note: Adjust the TTL (time to live) as needed
    redis_conn.setex(cache_key, 3600, json.dumps(questions))
    
    return questions
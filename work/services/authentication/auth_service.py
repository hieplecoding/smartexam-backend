# Assuming hash_password remains synchronous since it's CPU-bound
import re
import boto3
import aioboto3
import json
import os
import jwt
import json
import httpx
from starlette.requests import Request
from datetime import datetime, timedelta
from typing import Tuple
from jwt import PyJWTError
from jinja2 import Environment, FileSystemLoader, select_autoescape
from botocore.exceptions import ClientError
from work.utilities.redis_util import get_redis_connection
from work.utilities.security import hash_password, verify_password
from work.repositories.authentication.auth_repo import (
    check_user_verify,
    create_or_update_user_by_google_info,
    create_user,
    get_email_by_email,
    get_subscription_status,
    get_token,
    get_user_by_identifier,
    get_user_by_user_id,
    get_user_by_username,
    get_user_information_by_user_id,
    insert_trial_repo,
    update_user_password,
    verify_repo,
    save_reset_password_token,
    get_email_by_username_or_email,
    get_user_by_reset_password_token,
    invalidate_reset_password_token,
    get_subscription_info_repo,
)
from work.utilities.token import (
    create_access_token,
    SECRET_KEY,
    ALGORITHM,
    create_access_token_google_user,
)
from fastapi import Depends, HTTPException, status

# setup for google
CLIENT_ID = os.environ.get("CLIENT_ID")

CLIENT_SECRET = os.environ.get("CLIENT_SECRET")
REDIRECT_URI = os.environ.get("REDIRECT_URI")
# Google's OAuth 2.0 server endpoints
TOKEN_URL = "https://oauth2.googleapis.com/token"


async def register_user(username: str, password: str, email: str, db):
    # Regular expression for validating an Email
    email_regex = r"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"

    # First, check if the username already exists in the database.
    existing_user = await get_user_by_username(username, db)

    # If the username already exists, return a message or a boolean indicating failure.
    if existing_user is not None:
        return (
            False,
            "Tên đăng nhập đã được sử dụng. Vui lòng chọn lại tên đưang nhập khác!",
        )

    # Validate the email address using regex
    if not re.match(email_regex, email):
        return False, "Tên email không hợp lệ"

    # If the username does not exist and the email is valid, proceed with creating the user.
    hashed_password = hash_password(password)
    user_created = await create_user(username, hashed_password, email, db)
    if not user_created:
        return False, "Xảy ra lỗi trong lúc tạo tài khoản"
    return True, "Người dùng đã được tạo thành công"


async def authenticate_user(identifier: str, password: str, db):
    user = await get_user_by_identifier(identifier, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Không tìm thấy tên đăng nhập hoặc email {identifier}.",
        )
    if not await check_user_verify(identifier, db):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Tài khoản {identifier} chưa được xác nhận. Hãy kiểm tra lại hòm thư.",
        )

    if user and verify_password(password, user["password"]):
        await cache_user_information(user)
        return create_access_token({"sub": user["user_id"]})  #
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Sai mật khẩu hoặc tên đăng nhập. Hãy liên hệ Smart German Exam nếu cần sự trợ giúp.",
        )


async def cache_user_information(user):
    # Exclude sensitive data like passwords from the cached data
    user_info_to_cache = {
        "user_id": user["user_id"],
        "username": user["username"],
        "user_email": user["user_email"],
        # Using .get() for safety
        # Include any other non-sensitive user information here
    }

    # Initialize Redis connection
    redis_conn = get_redis_connection()
    cache_key = f"user:{user['user_id']}"

    # Serialize and cache the user data with a TTL (e.g., 3600 seconds = 1 hour)
    redis_conn.setex(cache_key, 3600, json.dumps(user_info_to_cache))


# Example modification for token_check_user_exits
async def token_check_user_exits(user_id: int, db) -> bool:
    user = await get_user_by_user_id(user_id, db)
    return user is not None


async def check_subscription_active(user_id: int, db) -> str:
    user = await get_user_by_user_id(user_id, db)
    if user:
        return await get_subscription_status(user["user_id"], db)
    return "not_found"


async def change_user_password(
    user_id: int, old_password: str, new_password: str, confirm_new_password: str, db
):
    # Get the user by username
    user = await get_user_by_user_id(user_id, db)
    if not user:
        return False, "Không tìm thấy người dùng."

    # Verify the old password
    if not verify_password(old_password, user["password"]):
        return False, "Mật khẩu cũ không chính xác."

    # Check if new password matches confirm password
    if new_password != confirm_new_password:
        return False, "Mật khẩu mới không trùng nhau."

    # Check if new password is different from the old password
    if verify_password(new_password, user["password"]):
        return False, "Mật khẩu mới phải khác mật khẩu cũ."

    # Hash the new password
    hashed_new_password = hash_password(new_password)

    # Update the user's password
    update_successful = await update_user_password(user_id, hashed_new_password, db)
    if update_successful:
        return True, "Thay đổi mật khẩu thành công."
    else:
        return False, "Thay đổi mật khẩu không thành công."


async def get_user_data_service(user_id: int, db):
    # Initialize Redis connection
    redis_conn = get_redis_connection()
    cache_key = f"user_data:{user_id}"

    # Attempt to fetch the user data from Redis cache
    cached_user_data = redis_conn.get(cache_key)
    if cached_user_data:
        return json.loads(cached_user_data)  # Deserialize the JSON stored data

    # If not in cache, fetch from database
    user = await get_user_information_by_user_id(user_id, db)
   
    if user:
        # Serialize and cache the user data with a TTL (e.g., 3600 seconds = 1 hour)
        redis_conn.setex(cache_key, 3600, json.dumps(user))

        # Return the user data
        return {
            "username": user["username"],
            "user_email": user["user_email"],
            "end_date": user["end_date"],
            "plan_name": user["plan_name"],
            # Add other user details as needed
        }
    else:
        # If user is not found, raise an HTTPException
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Không tìm thấy người dùng."
        )


async def send_email_service(user_email: str, token: str) -> Tuple[bool, str]:
    base_url = os.getenv("BASE_BACKEND_URL", "http://localhost:8000")
    verification_url = f"{base_url}/verify?token={token}"
    region_name = os.getenv("REGION_NAME")
    sender_email = os.getenv("SENDER_EMAIL")
    current_dir = os.path.dirname(__file__)
    
    # Setup the Jinja2 environment for HTML email templating
    env = Environment(loader=FileSystemLoader(current_dir), autoescape=select_autoescape(["html", "xml"]))
    template = env.get_template("email_template.html")
    html_email_content = template.render(verification_url=verification_url)

    # Create a new aioboto3 session
    session = aioboto3.Session()
    async with session.client("ses", region_name=region_name) as ses_client:
        try:
            response = await ses_client.send_email(
                Destination={"ToAddresses": [user_email]},
                Message={
                    "Body": {"Html": {"Charset": "UTF-8", "Data": html_email_content}},
                    "Subject": {"Charset": "UTF-8", "Data": "Your Email from Smart German Exam"},
                },
                Source=sender_email,
            )
            return True, f"Email sent! Message ID: {response['MessageId']}"
        except Exception as e:
            # It's good practice to log the full error details for debugging
            print(f"Error sending email: {e}")
            return False, f"Failed to send email: {str(e)}"


async def check_username_email(username, user_email, db):
    user = await get_user_by_username(username, db)
    if user:
        return False, "Tên đăng nhập đã được sự dụng."
    email = await get_email_by_email(user_email, db)
    if email:
        return False, "Email này đã được sử dụng"
    return True, "Username and password are not used."


async def get_token_to_verify(username: str, db):
    success, result = await get_token(username, db)
    if not success:
        return False, result  # Return False and the error message if unsuccessful
    return True, result  # Return True and the token if successful


async def verify_service(token, db):
    verify_success, user_id = await verify_repo(token, db)
    if not verify_success:
        return False, "Không thể xác nhận xảy ra lỗi"
    # Assuming verification was successful, initiate the trial period
    trial_success, trial_message = await initiate_trial_service(user_id, db)
    if not trial_success:
        # Log or handle the trial initiation error
        return False, trial_message  # Consider a more robust error handling strategy
    return True, "Đã xác nhận thành công và có thể sử dụng miễn phí 7 ngày"


# New service for initiating the trial period
async def initiate_trial_service(user_id: int, db):
    try:
        trial_plan_id = 4  # You'll need to implement this
        trial_start_date = datetime.utcnow()
        trial_end_date = trial_start_date + timedelta(days=7)
        trial_inserted = await insert_trial_repo(
            user_id, trial_plan_id, trial_start_date, trial_end_date, db
        )
        if trial_inserted:
            return True, "Gói dùng thử đã được thêm thành công."
        else:
            return False, "Xảy ra lỗi khi tạo gói dùng thử."
    except Exception as e:
        print(f"An error occurred while initiating the trial: {e}")
        return False, "Xảy ra lỗi khi tạo gói dùng thử."


async def send_reset_password_email(user_email: str, token: str) -> Tuple[bool, str]:
    base_url = os.getenv("BASE_BACKEND_URL", "http://localhost:8000")
    reset_password_url = f"{os.getenv('BASE_FRONTEND_URL')}/change-password?token={token}"
    region_name = os.getenv("REGION_NAME")
    sender_email = os.getenv("SENDER_EMAIL")
    current_dir = os.path.dirname(__file__)
    env = Environment(loader=FileSystemLoader(current_dir), autoescape=select_autoescape(["html", "xml"]))
    template = env.get_template("reset_password_template.html")
    html_email_content = template.render(reset_password_url=reset_password_url)

    session = aioboto3.Session()
    async with session.client("ses", region_name=region_name) as ses_client:
        try:
            response = await ses_client.send_email(
                Destination={"ToAddresses": [user_email]},
                Message={
                    "Body": {"Html": {"Charset": "UTF-8", "Data": html_email_content}},
                    "Subject": {"Charset": "UTF-8", "Data": "Reset Your Password - Smart German Exam"},
                },
                Source=sender_email,
            )
            return True, f"Email sent! Message ID: {response['MessageId']}"
        except Exception as e:
            return False, f"Failed to send email: {str(e)}"


async def get_user_email_by_username_or_email(
    username_or_email: str, db
) -> Tuple[bool, str]:
    user_email = await get_email_by_username_or_email(username_or_email, db)
    if user_email:
        return True, user_email
    else:
        return False, "Tên đăng nhập hoặc Email không tồn tại. Vui lòng kiểm tra lại!"


async def create_reset_password_token(username_or_email: str, db) -> Tuple[bool, str]:
    token_data = {"sub": username_or_email, "purpose": "reset_password"}
    expires_delta = timedelta(hours=0.5)  # Token expires in 30 mins

    token = create_access_token(data=token_data, expires_delta=expires_delta)

    # Decode the token to extract the 'exp' claim for expiration time
    decoded = jwt.decode(
        token, SECRET_KEY, algorithms=[ALGORITHM], options={"verify_signature": False}
    )
    expires_at = datetime.utcfromtimestamp(
        decoded["exp"]
    )  # Convert exp to a datetime object

    success = await save_reset_password_token(username_or_email, token, expires_at, db)
    if success:
        return True, token
    else:
        return False, "Failed to save the reset password token."


async def reset_user_password(token: str, new_password: str, db) -> Tuple[bool, str]:
    try:
        # Decode the JWT token to extract the payload
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username_or_email = payload.get("sub")
        if not username_or_email:
            return False, "Invalid token."

        # Check for the token's expiration and purpose
        if payload.get(
            "purpose"
        ) != "reset_password" or datetime.utcnow() > datetime.fromtimestamp(
            payload.get("exp")
        ):
            return False, "Token is expired or invalid."

        # Fetch the user associated with the reset password token
        user = await get_user_by_reset_password_token(token)
        if not user:
            # Instead of "User not found", indicate the link/token is no longer valid
            return False, "Link không còn khả dụng."

        # Hash the new password provided by the user
        hashed_password = hash_password(new_password)

        # Update the user's password in the database
        update_success = await update_user_password(
            user["user_id"], hashed_password, db
        )
        if not update_success:
            return False, "Thay đổi mật khẩu thất bại."

        # Invalidate the reset password token to prevent reuse
        token_invalidation_success = await invalidate_reset_password_token(
            user["username"], db
        )
        if not token_invalidation_success:
            print("Failed to invalidate reset password token.")  # Log the error

        # Return a success message
        return True, "Thay đổi mật khẩu thành công."
    except PyJWTError:
        # Catch any errors related to JWT token decoding
        return False, "Token is invalid."


async def get_user_info(access_token: str):
    url = "https://www.googleapis.com/oauth2/v2/userinfo"
    headers = {"Authorization": f"Bearer {access_token}"}
    async with httpx.AsyncClient() as client:
        response = await client.get(url, headers=headers)
    if response.status_code == 200:
        return response.json()
    else:
        raise HTTPException(
            status_code=response.status_code, detail="Failed to fetch user information"
        )


async def google_auth_service(request: Request):
    body = await request.json()
    access_token = body.get("access_token")
    if not access_token:
        raise HTTPException(status_code=400, detail="Access token missing")

    user_info = await get_user_info(access_token)

    if (
        not user_info
        or "id" not in user_info
        or "email" not in user_info
        or "name" not in user_info
    ):
        raise HTTPException(
            status_code=500, detail="Failed to obtain user information from Google"
        )

    google_id = user_info["id"]
    email = user_info["email"]
    name = user_info["name"]

    user_id = await create_or_update_user_by_google_info(google_id, name, email)

    if not user_id:
        raise HTTPException(status_code=500, detail="Failed to create or update user")

    # Generate a JWT token for the session, using the google_id
    access_token = create_access_token_google_user(
        {"sub": google_id}
    )  # 'sub' is a commonly used JWT claim indicating the subject of the token

    # Return the access token to the client for use in authentication headers
    return {"access_token": access_token, "token_type": "bearer"}


async def get_subscription_info_service(user_id: int, db):
    # Fetch user's subscription info from the repository
    return await get_subscription_info_repo(user_id, db)

async def send_contact_email_service(user_email: str, title: str, message: str) -> Tuple[bool, str]:
    region_name = os.getenv("REGION_NAME")
    sender_email = os.getenv("SENDER_EMAIL")
    recipient_email = "smartgermanexam@gmail.com"
    current_dir = os.path.dirname(__file__)

    # Setup Jinja2 environment for HTML email templating
    env = Environment(loader=FileSystemLoader(current_dir), autoescape=select_autoescape(["html", "xml"]))
    template = env.get_template("contact_email_template.html")
    html_email_content = template.render(user_email=user_email, title=title, message=message)

    # Create a new aioboto3 session
    session = aioboto3.Session()
    async with session.client("ses", region_name=region_name) as ses_client:
        try:
            response = await ses_client.send_email(
                Destination={"ToAddresses": [recipient_email]},
                Message={
                    "Body": {"Html": {"Charset": "UTF-8", "Data": html_email_content}},
                    "Subject": {"Charset": "UTF-8", "Data": title},
                },
                Source=sender_email,
            )
            return True, f"Email sent! Message ID: {response['MessageId']}"
        except Exception as e:
            # Logging the error is crucial for troubleshooting
            print(f"Error sending contact email: {e}")
            return False, f"Failed to send contact email: {str(e)}"
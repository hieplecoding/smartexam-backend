

from work.repositories.subscription.set_subscription_repo import check_username_or_email_repo, set_subscription_repo, subscription_plan_time_repo


async def check_username_or_email_service(identifier, db):
    return await check_username_or_email_repo(identifier, db)


async def set_subscription_service(user_id, subscription_plan_time, db):
    return await set_subscription_repo(user_id, subscription_plan_time, db)


async def subscription_plan_time_service(subscription_id, db):
    return await subscription_plan_time_repo(subscription_id, db)
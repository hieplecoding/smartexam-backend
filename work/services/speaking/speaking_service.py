import asyncio
from pathlib import Path
import shutil
from uuid import uuid4
from openai import OpenAI, AssistantEventHandler
import boto3
from fastapi import UploadFile, HTTPException
from io import BytesIO
import subprocess
# package to run in asynchronous ways to improve performance
import aiofiles
import aioboto3

import os
from work.utilities.openai_client import get_client

# Define the event handler for streaming responses
class EventHandler(AssistantEventHandler):    
  def on_tool_call_delta(self, delta, snapshot):
    if delta.type == 'code_interpreter':
      if delta.code_interpreter.input:
        print(delta.code_interpreter.input, end="", flush=True)
      if delta.code_interpreter.outputs:
        print(f"\n\noutput >", flush=True)
        for output in delta.code_interpreter.outputs:
          if output.type == "logs":
            print(f"\n{output.logs}", flush=True)

async def generate_audio(client, text, thread_id):
    try:
        print(f"\nFinal text for audio generation: {text}")
        bucket_name = "smart-exam-audio"
        folder_name = "thread_speaking"
        unique_id = uuid4().hex
        audio_file_name = f"{thread_id}_{unique_id}.mp3"
        s3_file_key = f"{folder_name}/{audio_file_name}"
        audio_file_path = Path(__file__).parent / audio_file_name

        # Generate the audio
        response = client.audio.speech.create(
            model="tts-1",
            voice="alloy",
            input=text
        )
        # Call stream_to_file synchronously within an async function using asyncio.to_thread
        await asyncio.to_thread(response.stream_to_file, str(audio_file_path))
        print(f"Audio response saved to {audio_file_path}")

        # Asynchronous upload to S3 using aioboto3
        session = aioboto3.Session()
        async with session.client('s3') as s3_client:
            async with aiofiles.open(audio_file_path, 'rb') as audio_file:
                await s3_client.upload_fileobj(
                    audio_file,
                    bucket_name,
                    s3_file_key,
                    ExtraArgs={
                        'ContentType': 'audio/mpeg'
                    }
                )

        audio_url = f"https://{bucket_name}.s3.amazonaws.com/{s3_file_key}"
        print(f"Audio file uploaded to {audio_url}")

        # Optionally, delete the local file if no longer needed
        await  asyncio.to_thread(audio_file_path.unlink)
        return audio_url

    except Exception as e:
        print(e)
        return str(e), False


async def process_speaking_service(thread_id, text):
  try:
      
    client = get_client()
    assistant_key = os.environ.get("ASSISTANT_ID")
    if not assistant_key:
        return "There is not assistant key", False

    user_input = text
    if not user_input:
        return "There is no user input", False

    thread_id = thread_id
    if not thread_id:
        return "There is no thread id", False

    message = client.beta.threads.messages.create(
    thread_id=thread_id,
    role="user",
    content=user_input
    )

    with client.beta.threads.runs.stream(
    thread_id=thread_id,
    assistant_id=assistant_key,
    instructions="Du redest auf Deutsch. Du musst kurze Rückmeldung geben und Sie müssen nicht mehr als 2 Sätze sein.",
    event_handler=EventHandler(),
    ) as stream:
        stream.until_done()

    lastMessageAfterRun = client.beta.threads.messages.list(thread_id, order='desc', limit=1)

    if hasattr(lastMessageAfterRun, 'data') and len(lastMessageAfterRun.data) > 0 and len(lastMessageAfterRun.data[0].content) > 0:
        content_block = lastMessageAfterRun.data[0].content[0]
        if content_block.type == 'text' and hasattr(content_block, 'text') and hasattr(content_block.text, 'value'):
            message_text = content_block.text.value
            audio_url = await generate_audio(client, message_text, thread_id)
            return audio_url, True
        else:
            return "There are some error while conversation", False
    else:
        return "There are some error while conversation", False


  except Exception as e:
      print(e)
      return str(e), False


async def process_speech_to_text_service(audio_file: UploadFile):
    try:
        client = get_client()
        print(f"Filename: {audio_file.filename}")
        print(f"Content-Type: {audio_file.content_type}")

        # Get the parent directory of the current Python file
        parent_dir = Path(__file__).resolve().parent

        # Construct the temporary file path next to the speaking_service.py file
        original_path = parent_dir / f"tmp_{audio_file.filename}"
        wav_path = parent_dir / f"tmp_{audio_file.filename}.wav"

        # Save the uploaded file to a temporary file
        with open(original_path, "wb") as tmp_file:
            shutil.copyfileobj(audio_file.file, tmp_file)
        print(f"Audio file saved to {original_path}")

        # Convert the file to WAV using FFmpeg
        subprocess.run(['ffmpeg', '-i', str(original_path), '-ar', '16000', '-ac', '1', str(wav_path)], check=True)
        print(f"Converted audio to WAV at {wav_path}")

        # Open the converted WAV file and process it using OpenAI's Whisper model
        with open(wav_path, "rb") as audio_speak_file:
            transcript = client.audio.transcriptions.create(
                file=audio_speak_file,
                model="whisper-1",
                response_format="verbose_json",
                timestamp_granularities=["word"]
            )

        # Cleanup temporary files
        os.remove(original_path)
        os.remove(wav_path)
        print(f"Temporary files deleted")

        return transcript.text

    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail=f"Failed to process the audio file: {str(e)}")

import os

from work.utilities.openai_client import get_client

async def process_create_thread_service():
    try:
        client = get_client()

        assistant_key = os.environ.get("ASSISTANT_ID")
        if not assistant_key:
            return "There is not assistant key", False
        thread = client.beta.threads.create()
        thread_id = thread.id
        if not thread_id:
            return "Some problem while creating the thread id", False
        return thread_id, True
    except Exception as e:
        return str(e), False
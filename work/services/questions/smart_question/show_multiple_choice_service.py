from typing import List
from work.models.smart_multiple_choice import SmartQuestion
from work.repositories.questions.smart_questions.show_multiple_choice_repo import show_questions_repo

async def process_show_multiple_choice_question() -> List[SmartQuestion]:
    question_rows = await show_questions_repo()
    
    # Ensure question_rows is not None before iterating
    if question_rows is None:
        return []
    
    questions = []
    for row in question_rows:
        question = SmartQuestion(
            id_question=row['id_question'],
            information_text=row['information_text'],
            question=row['question'],
            answer_1=row['answer_1'],
            answer_2=row['answer_2'],
            answer_3=row['answer_3'],
            correct_answer=row["correct_answer"],
            level_id=row['level_id']
        )
        questions.append(question.dict())  # .dict() if returning as a dict is preferred

    return questions


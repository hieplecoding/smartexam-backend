import openai

from work.repositories.questions.smart_questions.smart_multiple_choice_repo import (
    get_question_by_id,
    check_right_answer,
)


async def process_smart_multiple_choice_controller(id_question, user_answer):

    check_result, is_correct = await check_right_answer(id_question, user_answer)

    if is_correct:
        return check_result, is_correct

    question = await get_question_by_id(id_question)

    if not question:
        return "Question not found", False

    # Extracting details from the question row
    print(question["id_question"])

    # Constructing the prompt for OpenAI API using the refined approach
    prompt = (
        f"A German teacher, fluent in both German and Vietnamese, is explaining the nuances of the German language. The task involves analyzing a sentence from a German language exercise to identify and explain the correct answer among the given options. The sentence in question is '{question['information_text']}'. The options provided are:\n"
        f"A: {question['answer_1']}\n"
        f"B: {question['answer_2']}\n"
        f"C: {question['answer_3']}\n"
        "The teacher will outline why the selected answer is correct by referring to German grammar rules, including any common mistakes that learners might make related to this question. Additionally, the teacher will provide helpful tips for understanding and remembering the correct usage in similar contexts. After explaining in German, the teacher will then translate the explanation into Vietnamese, making it accessible for Vietnamese speakers learning German.\n"
        "This approach aims not only to correct the answer but also to deepen the learner's understanding of German grammar, vocabulary, and syntax, thereby enhancing their language skills in both German and Vietnamese.\n"
    )

    # Sending the prompt to OpenAI API, asking for a correction and explanation
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {
                "role": "system",
                "content": (
                    "You are a language expert fluent in Vietnamese. Provide feedback on the grammar question in Vietnamese only, "
                    "limiting your response to no more than 5 lines. Offer a correction and a brief explanation where necessary, "
                    "using clear and educational language. Avoid using dialects, colloquial language, or extensive background information. "
                    "Be concise and direct in your feedback.\n" + prompt
                ),
            },
            {
                "role": "user",
                "content": f"Answer: {user_answer}",
            },
        ],
    )

    # Return the AI's explanation or correction, and whether the initial user answer was correct
    return response.choices[0].message, True

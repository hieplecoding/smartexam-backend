from typing import List
import json 
from work.utilities.redis_util import get_redis_connection
from work.models.smart_multiple_choice import ListeningQuestion
from work.repositories.questions.listening_questions.smart_listening_question_repo import show_listening_questions_repo



async def process_show_listening_question(level, db) -> List[ListeningQuestion]:
    redis_conn = get_redis_connection()
    cache_key = f"listening_questions: {level}"
    # Try to fetch the questions from cache
    cached_questions = redis_conn.get(cache_key)
    if cached_questions:
        # If found in cache, return them after deserializing
        return json.loads(cached_questions)
    
    question_rows = await show_listening_questions_repo(level, db)
    # Ensure question_rows is not None before iterating
    if question_rows is None:
        return []

    listening_questions = []
    for row in question_rows:
        question = ListeningQuestion(
            id_question=row['id_question'],
            information_text=row['information_text'],
            question=row['question'],
            answer_1=row['answer_1'],
            answer_2=row['answer_2'],
            answer_3=row['answer_3'],
            audio_url=row['audio_url'], 
            correct_answer=row['correct_answer'],
            level_id=row['level_id'],
            art=row['art'], 
            question_type=row['question_type']
        )
        listening_questions.append(question.dict())  # .dict() if returning as a dict is preferred
    redis_conn.setex(cache_key, 3600, json.dumps(listening_questions))
    return listening_questions
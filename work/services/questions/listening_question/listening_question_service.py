import openai

from work.repositories.questions.listening_questions.listening_question_repo import (
    
    check_right_listening,
    check_right_listening_text,
    get_listening_question_by_id,
)


async def process_listening_question(id_question, user_answer, db):
    # check art, if text then use gpt, if dialog just show the right answer 
    #check_art = await check_art_question(id_question)
    question = await get_listening_question_by_id(id_question, db)    
    if not question:
            return "Question not found", False
    check_art = question['art']
    if(check_art == 'dialog'):
        check_result, is_correct = await check_right_listening(id_question, user_answer, db)
        # Return the result directly without further processing
        return check_result, is_correct
    elif(check_art == 'text'):
        check_result, is_correct = await check_right_listening_text(id_question, user_answer, db)
        if is_correct:
            return check_result, is_correct

        

        # Extracting details from the question row
        

        # Constructing the prompt for OpenAI API
        prompt = (
            "Đây là một bài tập nghe tiếng Đức. Dưới đây là câu hỏi và các câu trả lời có thể có được rút ra từ tài liệu nghe:\n\n"
            f"Đoạn thông tin: {question['information_text']}\n\n"
            f"Câu hỏi: {question['question']}\n\n"
            "Các phương án trả lời:\n"
            f"A: {question['answer_1']}\n"
            f"B: {question['answer_2']}\n"
            f"C: {question['answer_3']}\n\n"
            "Dựa vào văn bản thông tin được cung cấp, hãy xác định độ chính xác của các câu trả lời đã cho. Sử dụng câu sau từ tài liệu nghe như là tài liệu tham khảo cho nhiệm vụ này:\n"
            f"'{question['information_text']}'\n\n"
            f"'{question['correct_answer']}'\n\n"
            "Sử dụng văn bản thông tin và câu trả lời đúng này làm cơ sở để đánh giá độ chính xác của câu trả lời của người dùng và chỉ trả lời câu hỏi, đồng thời cung cấp giải thích vì sao bất kỳ câu trả lời sai nào là không chính xác."
        )

        # Sending the prompt to OpenAI API
        response = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[
                {
                    "role": "system",
                    "content": (
                        "Dựa vào văn bản thông tin được cung cấp, giải thích tại sao câu trả lời được chọn là sai, sử dụng lý do rõ ràng và ngắn gọn. Và cung cấp câu trả lời bằng tiếng Việt"
                        + prompt
                    ),
                },
                {
                    "role": "user",
                    "content": f"Answer: {user_answer}",
                },
            ],
        )

        return response.choices[0].message, True    
    else:
        return "Invalid art type or question not found", False
        

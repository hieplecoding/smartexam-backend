from typing import List, Tuple
import json 
from work.models.smart_multiple_choice import GrammarQuestion
from work.repositories.questions.grammar_questions.grammar_question_repo import check_right_grammar_answer, show_grammar_questions_repo, get_grammar_question_by_id
from work.utilities.redis_util import get_redis_connection

async def process_show_grammar_question(question_topic, db) -> List[GrammarQuestion]:
    redis_conn = get_redis_connection()
    cache_key = f"grammar_questions:{question_topic}"
    cached_questions = redis_conn.get(cache_key)
    if cached_questions:
        return json.loads(cached_questions)
    
    question_rows = await show_grammar_questions_repo(question_topic, db)
    if not question_rows:
        return []
    
    questions = [GrammarQuestion(
        id_question=row['id_question'],
        information_text=row['information_text'],
        question=row['question'],
        answer_1=row['answer_1'],
        answer_2=row['answer_2'],
        answer_3=row['answer_3'],
        correct_answer=row["correct_answer"],
        level_id=row['level_id'],
        topic=row['topic'],
        question_type=row['question_type']
    ).dict() for row in question_rows]  # Using dict for serialization
    redis_conn.setex(cache_key, 3600, json.dumps(questions))
    return questions

async def process_grammar_question_service(id_question: int, user_answer: str, db) -> Tuple[str, bool]:
    # Fetch the question details from the database
    question = await get_grammar_question_by_id(id_question, db)
    if not question:
        return "Question not found", False

    # Use the check_right_grammar_answer function to determine if the answer is correct
    result_message, is_correct = await check_right_grammar_answer(id_question, user_answer, db)

    return result_message, is_correct
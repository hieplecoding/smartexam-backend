from work.repositories.essay.add_writing_question import add_writing_question
from work.repositories.questions.grammar_questions.add_grammar_question import add_grammar_question
from work.repositories.questions.smart_questions.add_smart_question_repo import add_smart_question

from work.repositories.questions.listening_questions.add_listening_question_repo import add_listening_question

async def process_add_smart_question(question, db):
    return await add_smart_question(question, db)

async def process_add_listening_question(question, db):
    return await add_listening_question(question, db)

async def process_add_grammar_question(question, db):
    return await add_grammar_question(question, db)

async def process_add_writing_question(question, db):
    return await add_writing_question(question, db)

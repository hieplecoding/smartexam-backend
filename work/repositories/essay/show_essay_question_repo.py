from typing import List, Optional
from work.db_connect.db_connect import create_connection


async def show_writing_question_repo(level_description: str, db) -> Optional[List[dict]]:
    try:
        query = """
        SELECT wq.* FROM writing_questions wq
        JOIN question_level ql ON wq.level_id = ql.id
        WHERE ql.level = $1
        """
        records = await db.fetch(query, level_description)
        return [dict(record) for record in records]
    except Exception as e:
        print(f"An error occurred while fetching questions: {e}")
        return None




async def get_writing_question(question_id: int, db) -> Optional[dict]:
    try:
        query = """
        SELECT * FROM writing_questions WHERE question_id = $1
        """
        question_row = await db.fetchrow(query, question_id)

        if question_row:
            return dict(question_row)
    except Exception as e:
        print(f"An error occurred while fetching the question: {e}")
        return None

# alo alo alo alo
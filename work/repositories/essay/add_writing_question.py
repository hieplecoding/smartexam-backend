from work.db_connect.db_connect import create_connection
from typing import Dict, Any

from work.models.essay import EssayExam
from work.utilities.redis_util import get_redis_connection

async def add_writing_question(question: EssayExam, db) -> Dict[str, Any]:
    
    try:
        # Prepare the INSERT statement with the correct table and column names
        query = """
        INSERT INTO writing_questions (information_text, level_id)
        VALUES ($1, $2)
        RETURNING question_id;
        """
        # Execute the query and get the new question ID
        record = await db.fetchrow(query, question.information_text, question.level_id)
        redis_conn = get_redis_connection()
        cache_key = f"writing_questions"
        redis_conn.delete(cache_key)
        # Confirm insertion and return the new question ID
        return {"id_question": record["question_id"], "detail": "Writing question added successfully"}
    except Exception as e:
        # Handle any errors that occur during the insert operation
        print(f"An error occurred while inserting the question: {e}")
        return None





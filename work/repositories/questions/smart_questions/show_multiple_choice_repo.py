# repositories/show_multiple_choice_repo.py

import json
from work.db_connect.db_connect import create_connection
from typing import List, Optional

# Repository: Use Redis for caching
async def show_questions_repo() -> List[dict]:
    try:
        # Attempt to fetch cached data
        

        # Fetch from database if not cached
        conn = await create_connection()
        records = await conn.fetch("SELECT * FROM smart_multiple_question")

        if records:
            questions = [dict(record) for record in records]
            # Cache the fetched questions
            
            return questions
        else:
            return []  # Return an empty list if no records found
    except Exception as e:
        print(f"An error occurred while fetching questions: {e}")
        return []  # Return an empty list in case of error
    finally:
        if 'conn' in locals():
            await conn.close()



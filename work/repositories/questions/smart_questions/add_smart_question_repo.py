# repositories/add_smart_question_repo.py

from work.db_connect.db_connect import create_connection
from work.models.smart_multiple_choice import SmartQuestion
from typing import Dict, Any

async def add_smart_question(question: SmartQuestion, db) -> Dict[str, Any]:
    
    try:
        # Prepare the INSERT statement
        query = """
        INSERT INTO smart_multiple_question (information_text, question, answer_1, answer_2, answer_3, correct_answer, level_id)
        VALUES ($1, $2, $3, $4, $5, $6, $7)
        RETURNING id_question;
        """
        # Execute the query and get the new question ID
        record = await db.fetchrow(query, question.information_text, question.question, question.answer_1, question.answer_2, question.answer_3, question.correct_answer, question.level_id)
        
        # Confirm insertion and return the new question ID
        return {"id_question": record["id_question"], "detail": "Smart question added successfully"}
    except Exception as e:
        # Handle any errors that occur during the insert operation
        print(f"An error occurred while inserting the question: {e}")
        return None
   

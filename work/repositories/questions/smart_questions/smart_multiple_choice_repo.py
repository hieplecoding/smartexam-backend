

from work.db_connect.db_connect import create_connection
from typing import Optional


async def get_question_by_id(id_question: int) -> Optional[dict]:
    conn = await create_connection()
    try:
        # Use fetchrow to get a single row. If the question exists, it's returned as a record
        question_row = await conn.fetchrow(
            "SELECT * FROM smart_multiple_question WHERE id_question = $1", id_question
        )

        # Convert the asyncpg Record to a dict if a row was found
        if question_row:
            return {
                "id_question": question_row["id_question"],
                "information_text": question_row["information_text"],
                "question": question_row["question"],
                "answer_1": question_row["answer_1"],
                "answer_2": question_row["answer_2"],
                "answer_3": question_row["answer_3"],
                "correct_answer": question_row["correct_answer"]
                # Include any other fields you have in your table
            }
    except Exception as e:
        print(f"An error occurred while fetching the question: {e}")
        return None
    finally:
        await conn.close()

async def check_right_answer(id_question: int, user_answer: str) -> str:
    question = await get_question_by_id(id_question)
    if not question:
        return "Question not found", False

    correct_answer = question['correct_answer']
    

    if user_answer.lower() == correct_answer.lower():
        return "Es ist richtig", True
    else:
        return None, False
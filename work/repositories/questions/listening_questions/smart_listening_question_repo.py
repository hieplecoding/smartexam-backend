


import json
from typing import List, Optional
from work.db_connect.db_connect import create_connection


async def show_listening_questions_repo(level, db) -> Optional[List[dict]]:
    
    try:
        # Attempt to fetch cached data from Redis
       
        

        # If not cached, fetch from the database
        query = """
        SELECT lq.* FROM listening_question lq
        JOIN question_level ql ON lq.level_id = ql.id
        WHERE ql.level = $1
        """
        records = await db.fetch(query, level)
        
        # Check if records were found
        if records:
            questions = [dict(record) for record in records]
            
            
            
            return questions
        else:
            # Return an empty list if no records are found
            return []
    except Exception as e:
        print(f"An error occurred while fetching listening questions: {e}")
        # Return an empty list in case of error, ensuring the function always returns a list
        return []
    
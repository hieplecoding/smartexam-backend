from typing import Optional
from work.db_connect.db_connect import create_connection


async def get_listening_question_by_id(id_question: int, db) -> Optional[dict]:
 
    try:
        # Use fetchrow to get a single row. If the question exists, it's returned as a record
        question_row = await db.fetchrow(
            "SELECT * FROM listening_question WHERE id_question = $1", id_question
        )

        # Convert the asyncpg Record to a dict if a row was found
        if question_row:
            return {
                "id_question": question_row["id_question"],
                "information_text": question_row["information_text"],
                "question": question_row["question"],
                "answer_1": question_row["answer_1"],
                "answer_2": question_row["answer_2"],
                "answer_3": question_row["answer_3"],
                "audio_url": question_row["audio_url"],
                "correct_answer": question_row["correct_answer"],
                "art":question_row["art"]
                # Include any other fields you have in your table
            }
    except Exception as e:
        print(f"An error occurred while fetching the question: {e}")
        return None
    

async def check_right_listening(id_question: int, user_answer: str, db) -> str:
    question = await get_listening_question_by_id(id_question, db)
    if not question:
        return "Question not found", False

    correct_answer = question['correct_answer']
    

    if not correct_answer:
        return "Question not found", False

    if user_answer.lower() == correct_answer.lower():
        return "Es ist richtig", True
    else:
        return "Es ist falsch", True

async def check_right_listening_text(id_question: int, user_answer: str, db) -> str:
    question = await get_listening_question_by_id(id_question, db)
    if not question:
        return "Question not found", False

    correct_answer = question['correct_answer']
    

    if not correct_answer:
        return "Question not found", False

    if user_answer.lower() == correct_answer.lower():
        return "Es ist richtig", True
    else:
        return "Es ist falsch", False



from work.db_connect.db_connect import create_connection
from typing import List, Optional


async def show_grammar_questions_repo(question_topic, db):
    try:
        # Corrected to use the `db` parameter for database operations
        query = "SELECT * FROM grammar_questions WHERE topic = $1"
        records = await db.fetch(query, question_topic)

        if records:
            questions = [dict(record) for record in records]
            return questions
        else:
            return []  # Return an empty list if no records found
    except Exception as e:
        print(f"An error occurred while fetching questions: {e}")
        return []  # Return an empty list in case of error

async def get_grammar_question_by_id(id_question: int, db):
    try:
        # Using fetchrow to fetch a single record directly, which simplifies processing
        query = """
        SELECT * FROM grammar_questions WHERE id_question = $1;
        """
        question = await db.fetchrow(query, id_question)
        return question
    except Exception as e:
        print(f"An error occurred fetching grammar question by ID: {e}")
        return None

async def check_right_grammar_answer(id_question: int, user_answer: str, db):
    question = await get_grammar_question_by_id(id_question, db)
    if not question:
        return "Question not found", False
    if question['question_type'] == 'multiple_choice':
        # Extract the last part from correct_answer (e.g., 'answer_1' -> '1') and use it to get the correct option
        answer_field = f"answer_{question['correct_answer'].split('_')[-1]}"
        correct_option = answer_field
        is_correct = (user_answer.lower() == correct_option.lower())
    elif question['question_type'] == 'fill_blank':
        is_correct = (user_answer.lower() == question['correct_answer'].lower())

    return ("Es ist richtig" if is_correct else "Es ist falsch"), is_correct


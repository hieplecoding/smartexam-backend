from work.db_connect.db_connect import create_connection
from work.models.smart_multiple_choice import GrammarQuestion
from typing import Dict, Any

from work.utilities.redis_util import get_redis_connection

async def add_grammar_question(question: GrammarQuestion, db) -> Dict[str, Any]:
    
    try:
        # Prepare the INSERT statement with the correct table and column names
        query = """
        INSERT INTO grammar_questions (information_text, question, answer_1, answer_2, answer_3, correct_answer, level_id, topic, question_type)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
        RETURNING id_question;
        """
        # Execute the query and get the new question ID
        record = await db.fetchrow(query, question.information_text, question.question, question.answer_1, question.answer_2, question.answer_3, question.correct_answer, question.level_id, question.topic, question.question_type)
        redis_conn = get_redis_connection()
        cache_key = f"grammar_questions"
        redis_conn.delete(cache_key)
        # Confirm insertion and return the new question ID
        return {"id_question": record["id_question"], "detail": "Grammar question added successfully"}
    except Exception as e:
        # Handle any errors that occur during the insert operation
        print(f"An error occurred while inserting the question: {e}")
        return e
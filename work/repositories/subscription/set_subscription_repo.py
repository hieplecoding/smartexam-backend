
from work.utilities.redis_util import get_redis_connection


async def check_username_or_email_repo(identifier: str, db) -> int:
    try:
        user = await db.fetchrow("""
            SELECT user_id FROM users
            WHERE username = $1 OR user_email = $1
        """, identifier)
        
        return user['user_id'] if user else None
    except Exception as e:
        print(f"Database query failed: {e}")
        return None

async def subscription_plan_time_repo(subscription_id, db):
    try:
        subscription_detail = await db.fetchrow("""
            SELECT plan_id, duration_months
            FROM subscription_plans
            WHERE plan_id = $1
        """, subscription_id)
        return subscription_detail  # This includes plan_id and duration_months
    except Exception as e:
        print(f"Failed to fetch subscription plan details: {e}")
        return None




async def set_subscription_repo(user_id: int, subscription_plan_time, db) -> (bool, str):  # type: ignore
    try:
        if not subscription_plan_time:
            return False, "Subscription plan not found."

        # Extracting necessary details from the subscription_plan_time
        plan_id = subscription_plan_time['plan_id']
        duration_months = subscription_plan_time['duration_months']

        # Fetch username using user_id
        username = await db.fetchval('SELECT username FROM users WHERE user_id = $1', user_id)
        if not username:
            return False, "User not found."

        # Initialize Redis connection
        redis_conn = get_redis_connection()

        # Construct the cache key and delete it
        cache_key = f"user_data:{user_id}"
        redis_conn.delete(cache_key)  # Ensure this matches your async Redis library's syntax for delete operation

        # Use a parameterized SQL query to update the user's subscription
        await db.execute("""
            UPDATE user_subscriptions
            SET 
                plan_id = $2, 
                end_date = start_date + INTERVAL '1 month' * $1, 
                status = 'active'
            WHERE user_id = $3
        """, duration_months, plan_id, user_id)

        return True, "Subscription successfully updated."
    except Exception as e:
        # It's generally a good practice to log the exact error in real applications
        print(f"Failed to update subscription: {e}")
        return False, "An error occurred while updating the subscription."








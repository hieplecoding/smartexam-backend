# work/repositories/auth_repo.py

import asyncpg
from work.db_connect.db_connect import create_connection
from datetime import datetime, timedelta
import jwt
import os
from dotenv import load_dotenv
import pytz
import json
from work.utilities.redis_util import get_redis_connection
from datetime import datetime, timezone

# Load environment variables from .env file
load_dotenv()
SECRET_KEY = os.environ.get("KEY_TOKEN") 
ALGORITHM = "HS256"


async def create_user(username: str, hashed_password: str, email: str, db):
  

    # Define the token expiration time
    token_expires = datetime.utcnow() + timedelta(days=1)

    # Generate a JWT for email verification with the expiration time
    verification_token = generate_verification_token(email, token_expires)

    try:
        # Insert the user along with the verification token and its expiration time into the database
        await db.execute(
            "INSERT INTO users(username, password, user_email, email_verified, verification_token, verification_token_expires) VALUES($1, $2, $3, FALSE, $4, $5)",
            username, hashed_password, email, verification_token, token_expires
        )
        return True, verification_token
    except Exception as e:
        print(f"An error occurred: {e}")
        return False, ""
    

def generate_verification_token(email: str, exp: datetime) -> str:
    payload = {
        "email": email,
        "exp": exp
    }
    token = jwt.encode(payload, SECRET_KEY, algorithm=ALGORITHM)
    return token


async def get_user_by_username(username: str, db):
    try:
        user = await db.fetchrow(
            "SELECT * FROM users WHERE username = $1", username
        )
        return {
            "user_id": user["user_id"],
            "username": user["username"],
            "password": user["password"],
            "user_email": user["user_email"],
        } if user else None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None
async def get_user_by_user_id(user_id: int, db):
    try:
        user = await db.fetchrow(
            "SELECT * FROM users WHERE user_id = $1", user_id
        )
        return {
            "user_id": user["user_id"],
            "username": user["username"],
            "password": user["password"],
            "user_email": user["user_email"],
        } if user else None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

async def get_user_information_by_user_id(user_id: int, db):
    try:
        # SQL query to join users, user_subscriptions, and subscription_plans
        query = """
        SELECT 
            u.username, 
            u.user_email, 
            us.end_date, 
            sp.plan_name
        FROM 
            users u
        JOIN 
            user_subscriptions us ON u.user_id = us.user_id
        JOIN 
            subscription_plans sp ON us.plan_id = sp.plan_id
        WHERE 
            u.user_id = $1
        ORDER BY 
            us.end_date DESC
        LIMIT 1
        """
        # Execute the query
        user_info = await db.fetchrow(query, user_id)
        if user_info:
            return {
                "username": user_info["username"],
                "user_email": user_info["user_email"],
                "end_date": user_info["end_date"].isoformat(),
                "plan_name": user_info["plan_name"]
            }
        else:
            return None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None



async def get_subscription_status(user_id: int, db):
    try:
        subscription = await db.fetchrow(
            "SELECT status FROM user_subscriptions WHERE user_id = $1 ORDER BY end_date DESC LIMIT 1",
            user_id
        )
        return subscription['status'] if subscription else 'inactive'
    except Exception as e:
        print(f"An error occurred checking subscription status: {e}")
        return 'error'  # or 'inactive', depending on how you want to handle exceptions

async def get_user_by_identifier(identifier: str, db):
    try:
        user = await db.fetchrow(
            "SELECT * FROM users WHERE username = $1 OR user_email = $1", identifier
        )
        return {
            "user_id": user["user_id"],
            "username": user["username"],
            "password": user["password"],
            "user_email": user["user_email"],
        } if user else None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None


async def update_user_password(user_id: int, hashed_new_password: str, db):
    try:
        # Update the user's password using the user_id
        await db.execute(
            "UPDATE users SET password = $1 WHERE user_id = $2",
            hashed_new_password, user_id
        )
        return True
    except Exception as e:
        print(f"An error occurred: {e}")
        return False

    


async def get_email_by_email(user_email, db):
    
    try:
        user = await db.fetchrow(
            "SELECT * FROM users WHERE user_email = $1", user_email
        )
        return {
            "user_email": user["user_email"],
        } if user else None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None



async def get_token(username: str, db):
    
    try:
        user = await db.fetchrow(
            "SELECT verification_token, verification_token_expires FROM users WHERE username = $1",
            username
        )
        if user and user['verification_token']:
            # Ensure the current time is offset-aware and matches the database's timezone (e.g., UTC)
            current_time = datetime.now(pytz.utc)
            
            if current_time < user['verification_token_expires']:
                return True, user['verification_token']
            else:
                return False, "Token đã hết hạn"
        else:
            return False, "Không tìm được token để verify"
    except Exception as e:
        print(f"An error occurred: {e}")
        return False, "Lỗi khi truy cập cơ sở dữ liệu"
    



async def verify_repo(token: str, db) -> bool:
    
    try:
        # Attempt to find the user with the given verification token and where email is not yet verified.
        user = await db.fetchrow(
            "SELECT user_id FROM users WHERE verification_token = $1 AND email_verified = FALSE", token
        )
        
        # If a user is found with the provided token
        if user:
            # Update the user's email_verified status to TRUE and clear the verification_token
            await db.execute(
                "UPDATE users SET email_verified = TRUE, verification_token = NULL, verification_token_expires = NULL WHERE user_id = $1",
                user['user_id']
            )
            return True, user['user_id']
        else:
            return False, None
    except Exception as e:
        print(f"An error occurred: {e}")
        return False, None

# Repository function to insert a trial subscription
async def insert_trial_repo(user_id: int, plan_id: int, start_date: datetime, end_date: datetime, db) -> bool:
    try:
        await db.execute(
            "INSERT INTO user_subscriptions (user_id, plan_id, start_date, end_date, status) VALUES ($1, $2, $3, $4, 'active')",
            user_id, plan_id, start_date, end_date
        )
        return True
    except Exception as e:
        print(f"Failed to insert trial subscription: {e}")
        return False

    

async def check_user_verify(identifier: str, db) -> bool:
    try:
        # This query checks both the username and user_email columns to find the user.
        # It returns the user's email_verified status if a match is found, or None otherwise.
        query = "SELECT email_verified FROM users WHERE username = $1 OR user_email = $1"
        user = await db.fetchrow(query, identifier)
        
        # Return the email_verified status if the user is found, False otherwise.
        # This logic remains the same as before, relying on the fact that accessing 'email_verified'
        # will return either True, False, or None (if the user is not found),
        # and in a boolean context, both False and None evaluate to False.
        return user['email_verified'] if user else False
    except Exception as e:
        print(f"An error occurred: {e}")
        return False



async def save_reset_password_token(username_or_email: str, token: str, expires_at: datetime, db) -> bool:
    
    try:
        await db.execute("""
            UPDATE users
            SET reset_password_token = $1, reset_password_token_expires = $2
            WHERE username = $3 OR user_email = $3
        """, token, expires_at, username_or_email)
        return True
    except Exception as e:
        print(f"Failed to save reset password token: {e}")
        return False
    


async def get_user_by_username_or_email(username_or_email: str):
    conn = await create_connection()
    try:
        # Attempt to fetch the user by username or email
        user = await conn.fetchrow("""
            SELECT * FROM users WHERE username = $1 OR user_email = $1
        """, username_or_email)
        if user:
            return {
                "user_id": user["user_id"],
                "username": user["username"],
                "user_email": user["user_email"],
            }
        return None
    except Exception as e:
        print(f"Error fetching user by username or email: {e}")
        return None
    finally:
        await conn.close()

async def get_email_by_username_or_email(username_or_email: str, db) -> str:
    
    try:
        user = await db.fetchrow("""
            SELECT user_email FROM users
            WHERE username = $1 OR user_email = $1
        """, username_or_email)
        return user['user_email'] if user else None
    except Exception as e:
        print(f"Database query failed: {e}")
        return None
    

async def get_user_by_reset_password_token(token: str):
    conn = await create_connection()
    try:
        # Query to select the user based on the reset_password_token
        user = await conn.fetchrow("""
            SELECT * FROM users
            WHERE reset_password_token = $1
        """, token)

        if user:
            return {
                "user_id": user["user_id"],
                "username": user["username"],
                "user_email": user["user_email"],
            }
        return None
    except Exception as e:
        print(f"Failed to fetch user by reset password token: {e}")
        return None
    finally:
        await conn.close()

async def invalidate_reset_password_token(username: str, db) -> bool:
    
    try:
        await db.execute("""
            UPDATE users
            SET reset_password_token = NULL, reset_password_token_expires = NULL
            WHERE username = $1
        """, username)
        return True
    except Exception as e:
        print(f"Failed to invalidate reset password token: {e}")
        return False
    

async def get_user_by_google_id_or_email(google_id: str, email: str) -> dict:
    conn = await create_connection()
    try:
        user = await conn.fetchrow("""
            SELECT user_id, username, user_email FROM users
            WHERE google_id = $1 OR user_email = $2
        """, google_id, email)
        return dict(user) if user else None
    except Exception as e:
        print(f"Database query failed: {e}")
        return None
    finally:
        await conn.close()

async def create_or_update_user_by_google_info(google_id: str, name: str, email: str) -> int:
    conn = await create_connection()
    try:
        # Check if the user exists
        existing_user = await get_user_by_google_id_or_email(google_id, email)
        if existing_user:
            # Update the existing user if any google_id related info changes (for simplicity, just updating name here)
            await conn.execute("""
                UPDATE users
                SET username = $1
                WHERE user_id = $2
            """, name, existing_user['user_id'])
            return existing_user['user_id']
        else:
            # Create a new user
            user = await conn.fetchrow("""
                INSERT INTO users (username, user_email, google_id, password)
                VALUES ($1, $2, $3, 'not_set')  -- Assuming password is not needed for users signing in with Google
                RETURNING user_id
            """, name, email, google_id)
            return user['user_id']
    except Exception as e:
        print(f"Database operation failed: {e}")
        return None
    finally:
        await conn.close()


async def get_subscription_info_repo(user_id: int, db):
    try:
        redis_conn = get_redis_connection()
        
        # Attempt to get cached subscription info using user_id
        cached_subscription_info = redis_conn.get(f"subscription_info:{user_id}")
        if cached_subscription_info:
            return json.loads(cached_subscription_info)
        
        # Fetch subscription info directly using the user_id
        subscription_info = await db.fetchrow("""
            SELECT us.end_date, sp.plan_name
            FROM user_subscriptions us
            JOIN subscription_plans sp ON us.plan_id = sp.plan_id
            WHERE us.user_id = $1 AND us.status = 'active'
            ORDER BY us.end_date DESC
            LIMIT 1
        """, user_id)

        if not subscription_info:
            return None  # Or your preferred way of indicating no active subscription

        # Convert datetime to string in ISO 8601 format
        end_date_str = subscription_info['end_date'].isoformat() if subscription_info['end_date'] else None

        subscription_details = {
            'plan_name': subscription_info['plan_name'],
            'end_date': end_date_str
        }
        
        # Cache the result in Redis with a TTL, using user_id
        redis_conn.setex(f"subscription_info:{user_id}", 3600, json.dumps(subscription_details))
        
        return subscription_details

    except Exception as e:
        print(f"Failed to fetch subscription info: {e}")
        return None



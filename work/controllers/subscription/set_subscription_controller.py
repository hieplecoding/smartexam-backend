


from fastapi import HTTPException, status

from work.services.subscription.set_subscription_service import check_username_or_email_service, set_subscription_service, subscription_plan_time_service


async def set_subscription_controller(user, db):
    # Check user.identifier to find the user_id
    user_id = await check_username_or_email_service(user.identifier, db)
    if not user_id:
        # User not found
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, 
            detail="Không tìm thấy username hoặc email."
        )

    # Find subscription plan duration and plan_id
    subscription_plan_time = await subscription_plan_time_service(user.subscription_id, db)
    if not subscription_plan_time:
        # Subscription plan not found
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, 
            detail="không tìm thấy gói đăng ký."
        )

    # Attempt to update the user's subscription with the given plan
    # Within set_subscription_controller, assuming user_subscription_id is known
    success, message = await set_subscription_service(user_id, subscription_plan_time, db)

    if not success:
        # If updating subscription failed
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, 
            detail=message
        )

    # If all operations were successful, return a 200 status with a success message
    return {"status": "success", "message": "Gói đăng ký đã được thêm thành công."}
from fastapi import HTTPException,status
from work.services.authentication.auth_service import token_check_user_exits
from work.services.questions.grammar_question.grammar_question_service import process_grammar_question_service, process_show_grammar_question, process_show_level_grammar_question

async def show_grammar_level_question_controller(question_level,current_user):
    user_exist = await token_check_user_exits(current_user)  # This should be async if checking the user involves DB or external calls
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    
    questions = await process_show_level_grammar_question(question_level)
    if not questions:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No questions found ***")
    
    return questions
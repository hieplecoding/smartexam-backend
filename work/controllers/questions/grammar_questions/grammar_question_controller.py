from fastapi import HTTPException,status
from work.services.authentication.auth_service import check_subscription_active, token_check_user_exits
from work.services.questions.grammar_question.grammar_question_service import process_grammar_question_service, process_show_grammar_question


async def show_grammar_question_controllers(question_topic, current_user, db):
    if(question_topic == 'null'):
        return []
    user_exist = await token_check_user_exits(current_user, db)  # This should be async if checking the user involves DB or external calls
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    subscription_status = await check_subscription_active(current_user, db)
    if subscription_status != "active":
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Gói sử dụng đã hết hạn. Hãy liên hệ đội ngũ trợ giúp để thanh toán gói sử dụng mới.")
    questions = await process_show_grammar_question(question_topic, db)
    if not questions:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No questions found ***")
    
    return questions


async def process_grammar_question_controller(user_answer, current_user, db):
    user_exist = await token_check_user_exits(current_user, db)
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")

    response, success = await process_grammar_question_service(user_answer.id_question, user_answer.user_answer, db)
    if response == "Question not found":
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=response)
    return {"response": response}

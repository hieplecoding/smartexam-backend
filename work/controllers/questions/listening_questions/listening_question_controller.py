from fastapi import HTTPException,status
from work.services.authentication.auth_service import token_check_user_exits
from work.services.questions.listening_question.listening_question_service import process_listening_question

async def listening_question_controller(current_user: str, question, db):
    user_exist = await token_check_user_exits(current_user, db)  # This should be async if checking the user involves DB or external calls
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    
    response, success = await process_listening_question(question.id_question, question.user_answer, db)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}
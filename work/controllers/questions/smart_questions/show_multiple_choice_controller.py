from fastapi import HTTPException,status
from work.services.authentication.auth_service import check_subscription_active, token_check_user_exits

from work.services.questions.smart_question.show_multiple_choice_service import process_show_multiple_choice_question


async def show_multiple_choice_controller(current_user: str, db):
    # Adjusted to check existence and subscription status separately for clarity
    user_exist = await token_check_user_exits(current_user, db)
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Không tìm thấy thông tin đăng nhập.")
    
    # Assuming you have adjusted or created a new function that checks subscription status
    subscription_status = await check_subscription_active(current_user, db)
    if subscription_status != "active":
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Gói sử dụng đã hết hạn. Hãy liên hệ đội ngũ trợ giúp để thanh toán gói sử dụng mới.")

    questions = await process_show_multiple_choice_question()
    if not questions:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Không thể tìm thấy câu hỏi.")
    
    return questions
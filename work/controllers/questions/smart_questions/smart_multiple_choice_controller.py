from fastapi import HTTPException,status
from work.services.authentication.auth_service import token_check_user_exits
from work.services.questions.smart_question.smart_multiple_choice_service import process_smart_multiple_choice_controller

async def smart_multiple_choice_controller(current_user: str, question):
    user_exist = await token_check_user_exits(current_user)  # This should be async if checking the user involves DB or external calls
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    
    response, success = await process_smart_multiple_choice_controller(question.id_question, question.user_answer)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}

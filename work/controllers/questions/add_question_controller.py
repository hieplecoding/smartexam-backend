from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from work.models.essay import EssayExam
from work.models.smart_multiple_choice import GrammarQuestion, SmartQuestion, ListeningQuestion
from work.services.authentication.auth_service import token_check_user_exits
from work.services.questions.add_question_service import process_add_grammar_question, process_add_smart_question, process_add_listening_question, process_add_writing_question
from work.services.questions.grammar_question.grammar_question_service import process_show_grammar_question

async def add_question_controller(question_data, question_type, current_user, db):
    user_exist = await token_check_user_exits(current_user, db)
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    
    # if question_type == 'smart':
    #     question = SmartQuestion(**question_data)
    #     result = await process_add_smart_question(question, db)
    if question_type == 'listening':
        question = ListeningQuestion(**question_data)
        result = await process_add_listening_question(question, db)
    elif question_type == 'grammar':
        question = GrammarQuestion(**question_data)
        result = await process_add_grammar_question(question, db)
    elif question_type == 'writing':
        question = EssayExam(**question_data)
        result = await process_add_writing_question(question, db)
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Có lỗi ở add question")
    
    return result

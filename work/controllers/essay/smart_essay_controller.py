from fastapi import HTTPException, status

from work.services.authentication.auth_service import token_check_user_exits
from work.services.essay.smart_essay_service import process_checking_essay


async def checking_essay_controller(user_answer, way_to_check, current_user, db):
    user_exists = token_check_user_exits(current_user, db)
    
    if not user_exists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    response, success = await process_checking_essay(way_to_check,user_answer.question_id, user_answer.content, db)

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}
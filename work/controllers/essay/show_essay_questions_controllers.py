from fastapi import HTTPException, status
from work.services.authentication.auth_service import check_subscription_active, token_check_user_exits
from work.services.essay.show_essay_questions_service import process_show_writing_question



async def show_writing_questions_controllers(level: str, current_user: str, db):
    user_exist = await token_check_user_exits(current_user, db)  # Ensure this function accepts `db`
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    subscription_status = await check_subscription_active(current_user, db)
    if subscription_status != "active":
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Gói sử dụng đã hết hạn. Hãy liên hệ đội ngũ trợ giúp để thanh toán gói sử dụng mới.")
    questions = await process_show_writing_question(level, db)  # Ensure this function accepts `db`
    if not questions:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No questions found ***")
    
    return questions


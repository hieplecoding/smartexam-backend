from fastapi import HTTPException,status

from work.services.authentication.auth_service import token_check_user_exits
from work.services.speaking.create_thread_id_service import process_create_thread_service

async def process_create_thread_controller(current_user_id, db):
    user_exist = await token_check_user_exits(current_user_id, db)  # This should be async if checking the user involves DB or external calls
    if not user_exist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found ***")
    
    response, success = await process_create_thread_service()

    if not success:
        raise HTTPException(status_code=404, detail=response)

    return {"response": response}

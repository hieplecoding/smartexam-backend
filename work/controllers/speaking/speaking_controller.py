
from fastapi import HTTPException,status
from work.services.authentication.auth_service import token_check_user_exits
from work.services.speaking.speaking_service import process_speaking_service, process_speech_to_text_service
from work.utilities.openai_client import get_client



async def speaking_controller(thread_id, audio_file):
    
    
    
    # speech to text
    
    text = await process_speech_to_text_service(audio_file)
    print(f"The text from user{text}!!")
    if text == "":
        raise HTTPException(status_code=404, detail="No text could be extracted from the audio")

    response, success = await process_speaking_service(thread_id, text)

    if not success:
        raise HTTPException(status_code=404, detail=response)
    return {"response": response}

   
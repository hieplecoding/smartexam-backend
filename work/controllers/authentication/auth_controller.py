from fastapi import APIRouter, HTTPException, status, Body, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.responses import RedirectResponse
import os
import urllib
from starlette.requests import Request

from work.models.user import UserRegistration, UserLogin, PasswordChangeRequest, ForgotPasswordRequest, ResetPasswordRequest
from work.models.contact_details import ContactDetails
from work.services.authentication.auth_service import (
    check_username_email,
    get_token_to_verify,
    google_auth_service,
    register_user,
    authenticate_user,
    change_user_password,
    get_user_data_service,
    send_email_service,
    verify_service,
    create_reset_password_token, 
    send_reset_password_email,
    get_user_email_by_username_or_email,
    reset_user_password,
    get_subscription_info_service,
    send_contact_email_service
)
from work.utilities.authen_token import get_current_user

router = APIRouter()

@router.post("/register")
async def register_user_controller(user: UserRegistration, db):
    # check username and password are already in the database
    success_not, message = await check_username_email(user.username, user.user_email, db)
    if not success_not:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=message)

    # Register the user
    success, message = await register_user(user.username, user.password, user.user_email, db)
    
    if not success:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=message)

    # Generate a token for email verification
    token_success, token_or_message = await get_token_to_verify(user.username, db)
    if not token_success:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=token_or_message)

    # Attempt to send a confirmation email
    token_check, result = await get_token_to_verify(user.username, db)
    if not token_check:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=result,
        )
    email_sent, email_message = await send_email_service(user.user_email, result)
    if not email_sent:
        # If email sending failed, log the failure and decide on the action.
        # Here, we choose to log the issue and inform the user without failing the registration.
        print(email_message)  # Log the failure for internal tracking
        response_message = {
            "registration": "Bạn đã đăng ký thành công",
            "email_status": "Không thể gửi mail cho bạn. Hãy check lại email hoặc liên hệ với Smart German Exam team.",
        }
    else:
        response_message = {
            "registration": "Bạn đã đăng ký thành công. Hãy kiểm tra hòm thư để ấn vào email xác nhận tài khoản!!",
            "email_status": "Mail xác nhận được gửi cho bạn.",
        }

    return jsonable_encoder(response_message)


async def login_user_controller(user: UserLogin, db):
    token = await authenticate_user(user.identifier, user.password, db)
    # If authenticate_user doesn't raise an exception, it means authentication was successful
    return {"access_token": token, "token_type": "bearer"}



@router.post("/change-password")
async def change_password_controller(
    password_change: PasswordChangeRequest ,
    user_id: int ,
    db
):
    # Proceed to change the password
    success, message = await change_user_password(
        user_id, 
        password_change.old_password, 
        password_change.new_password,
        password_change.confirm_new_password,
        db
    )
    if success:
        return {"message": message}
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST if "not match" in message or "different" in message else status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail=message
        )

@router.get("/user-data")
async def get_user_data_controller(user_id: int, db):
    return await get_user_data_service(user_id, db)


@router.get("/verify")
async def verify_controller(token: str, db):
    success, message = await verify_service(token, db)
    base_url = os.getenv("BASE_FRONTEND_URL")  # Directly use BASE_URL from the environment

    if success:
        # Redirect to the frontend login page if verification is successful
        verification_status_url = f"{base_url}/login?verification=success"
    else:
        # Encode the message for URL parameter
        encoded_message = urllib.parse.quote(message)
        verification_status_url = f"{base_url}/handle-verify?message={encoded_message}"

    return RedirectResponse(url=verification_status_url)
    
@router.post("/forgot-password")
async def forgot_password(request: ForgotPasswordRequest, db):
    # Check if user exists
    user_exists, user_email_or_message = await get_user_email_by_username_or_email(request.username_or_email, db)
    if not user_exists:
        # If the user does not exist, return the message from the service and indicate no action was taken
        return {"message": user_email_or_message, "actionTaken": False}

    # Proceed only if user exists
    success, token = await create_reset_password_token(request.username_or_email, db)
    if not success:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to generate reset password token.")

    success, message = await send_reset_password_email(user_email_or_message, token)
    if not success:
        print(message)  # Log the failure for internal tracking
        return {"message": "Failed to send a reset password link", "actionTaken": False}
    
    return {"message": "Chúng tôi đã gửi một link để đổi mật khẩu đến tài khoản email của bạn.", "actionTaken": True}


async def reset_password(request: ResetPasswordRequest, db):
    success, message = await reset_user_password(request.token, request.new_password, db)   
    if not success:     
        raise HTTPException(status_code=400, detail=message)
    return {"message": message}


async def google_auth_controller(request: Request):
    return await google_auth_service(request)

async def get_subscription_info_controller(user_id: int, db):
    return await get_subscription_info_service(user_id, db)

@router.post("/send-contact-email")
async def handle_contact_email(contact_details: ContactDetails, db):
    user_email = contact_details.user_email
    title = contact_details.title
    message = contact_details.message

    email_sent, email_message = await send_contact_email_service(user_email, title, message)
    
    if not email_sent:
        print(email_message)  
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, 
                            detail="Failed to send contact email.")
    return {"message": "Email successfully sent!", "actionTaken": True}
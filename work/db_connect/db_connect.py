import asyncpg
import os

async def create_connection():
    try:
        conn = await asyncpg.connect(
            database=os.getenv("POSTGRES_DB"),
            user=os.getenv("POSTGRES_USER"),
            password=os.getenv("POSTGRES_PASSWORD"),
            host=os.getenv("POSTGRES_HOST"),
            port=os.getenv("POSTGRES_PORT")
        )
        print("Connection to PostgreSQL DB successful")
        return conn
    except Exception as e:
        print(f"The error '{e}' occurred")
        return None

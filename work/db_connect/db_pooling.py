import asyncpg
import os

# Global variable for the connection pool
db_pool = None

async def create_pool():
    global db_pool
    db_pool = await asyncpg.create_pool(
        database=os.getenv("POSTGRES_DB"),
        user=os.getenv("POSTGRES_USER"),
        password=os.getenv("POSTGRES_PASSWORD"),
        host=os.getenv("POSTGRES_HOST"),
        port=os.getenv("POSTGRES_PORT"),
        min_size=1,    # Set a reasonable minimum size
        max_size=5     # Ensure max_size is greater than or equal to min_size
    )

async def get_db():
    async with db_pool.acquire() as connection:
        yield connection

async def close_pool():
    await db_pool.close()
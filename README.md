# smartexam-backend


# Prepare to install every package in requirements.txt
pip3 install -r requirements.txt

# Start the Fast API server
uvicorn main:app --reload

# debug ... contact me
https://www.youtube.com/watch?v=X3SgEXhX11w&fbclid=IwAR0XpXHS4YW4Hh1RXcsq1emvzMoXfJFlgmheMahCKUMa8UGS55aUI0xH0Ws

# how to work with openai API
https://platform.openai.com/docs/quickstart?context=python

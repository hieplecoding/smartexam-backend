# import package
from fastapi import Body, FastAPI, Query, Depends
from dotenv import load_dotenv
import os
import openai
from typing import Dict, List
from fastapi import FastAPI, HTTPException, Depends, status, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from pydantic import BaseModel
from work.controllers.speaking.create_thread_id_controller import process_create_thread_controller
from work.controllers.speaking.speaking_controller import speaking_controller
from work.controllers.subscription.set_subscription_controller import set_subscription_controller
from work.db_connect.db_pooling import close_pool, create_pool, get_db



#alo develop

from work.controllers.essay.show_essay_questions_controllers import show_writing_questions_controllers
from work.controllers.essay.smart_essay_controller import checking_essay_controller
from work.controllers.questions.grammar_questions.grammar_question_controller import process_grammar_question_controller, show_grammar_question_controllers
from work.models.essay import EssayAnswer, EssayExam

# import from code
# models
from work.models.smart_multiple_choice import GrammarQuestion, ListeningQuestion, MultipleChoiceAnswerFromUser, MultipleChoiceAnswerFromUserListening, SmartQuestion

# authentication
from work.controllers.authentication.auth_controller import google_auth_controller, login_user_controller, register_user_controller, get_user_data_controller, verify_controller, forgot_password, reset_password, get_subscription_info_controller

# question
from work.controllers.questions.add_question_controller import add_question_controller
# smart question
from work.controllers.questions.smart_questions.show_multiple_choice_controller import show_multiple_choice_controller
from work.controllers.questions.smart_questions.smart_multiple_choice_controller import smart_multiple_choice_controller

# listening question
from work.controllers.questions.listening_questions.listening_question_controller import listening_question_controller
from work.controllers.questions.listening_questions.smart_listening_question_controller import show_listening_question_controller

# 
from work.models.user import GoogleAuthCode, UserLogin, UserRegistration, ForgotPasswordRequest, ResetPasswordRequest, UserSubscription
from work.db_connect.db_connect import create_connection
from work.utilities.authen_token import get_current_user
from work.models.contact_details import ContactDetails

# import the PasswordChangeRequest model
from work.models.user import UserLogin, UserRegistration, PasswordChangeRequest
# import the change password controller function (assuming you have created this)
from work.controllers.authentication.auth_controller import change_password_controller, handle_contact_email

from authlib.integrations.starlette_client import OAuth
from starlette.config import Config
from starlette.responses import RedirectResponse
import httpx
from starlette.requests import Request

from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.interval import IntervalTrigger
from datetime import datetime, timedelta
from pytz import timezone

from work.utilities.scheduler import deactivate_expired_subscriptions

# Fast API
app = FastAPI()
# Load environment variables from .env file
load_dotenv()

# redis


# scheduler

scheduler = AsyncIOScheduler()
# pooling database
@app.on_event("startup")
async def startup_event():
    await create_pool()
    # Schedule the deactivate_expired_subscriptions function to run every 24 hours
    scheduler.add_job(
        deactivate_expired_subscriptions,
        IntervalTrigger(hours=24),
    )
    scheduler.start()
    
    # Get the current time
    current_time = datetime.now()
    # Format the time as a string (for example, "2023-04-01 12:00:00")
    formatted_time = current_time.strftime("%Y-%m-%d %H:%M:%S")
    
    print(f"Task scheduled to deactivate expired subscriptions every 24 hours. Scheduler started at: {formatted_time}")

@app.on_event("shutdown")
async def shutdown_event():
    await close_pool()
    scheduler.shutdown(wait=True)  # Set wait=True to allow all running jobs to finish
    print("Scheduler shut down.")

# Set OpenAI API key
openai.api_key = os.environ.get("OPENAI_API_KEY")

# Set up CORS

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "http://localhost:5001",  # Containerized React app
        "http://localhost:3000",  # React development server
        "http://192.168.0.102:3000",  # If accessing from another device in the network
        "https://www.smart-german-exam.com", 
        "https://smartexam-frontend-844c18e3e39b.herokuapp.com"
        # Add any other origins as needed
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# test get
@app.get("/")
async def root():
    return {"message": "Hi from Smart Exam"}

# test database connection
@app.get("/test-db-connection")
async def test_db_connection():
    connection = create_connection()
    if connection is not None:
        return {"message": "Connection to PostgreSQL DB successful"}
    else:
        return {"message": "Failed to connect to PostgreSQL DB"}

# register and login
@app.post("/register")
async def register(user: UserRegistration = Body(...), db=Depends(get_db)):
    return await register_user_controller(user, db)

@app.post("/login")
async def login(user: UserLogin = Body(...), db=Depends(get_db)):
    return await login_user_controller(user, db)

# verify account 
@app.get("/verify")
async def verify(token: str = Query(...), db=Depends(get_db)):
    return await verify_controller(token, db)

# the subscription for users
@app.post("/set-subscription")
async def set_subscription(user: UserSubscription = Body(...), db=Depends(get_db)):
    return await set_subscription_controller(user, db)

# @app.get("/user-subscription-info")
# async def get_subscription_info(current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
#     return await get_subscription_info_controller(current_user_id, db)

#userData
@app.post("/change-password")
async def change_password(password_change: PasswordChangeRequest = Body(...), current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await change_password_controller(password_change, current_user_id, db)

# get user info and subscription info
@app.get("/user-data")
async def get_user_data(current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await get_user_data_controller(current_user_id, db)

# send email to user
@app.post("/forgot-password")
async def forgot_password_endpoint(request: ForgotPasswordRequest,  db=Depends(get_db)):
    return await forgot_password(request, db)
# reset password from the link in email
@app.post("/reset-password")
async def reset_password_endpoint(request: ResetPasswordRequest,  db=Depends(get_db)):
    return await reset_password(request, db)

# google


@app.post("/google-auth")
async def google_auth(request: Request):
    return await google_auth_controller(request)
    


##
# smart multiple choice 
# show question for users to choose
# @app.get("/show_multiple_choice", response_model=List[SmartQuestion])
# async def show_multiple_choice(current_user: str = Depends(get_current_user)):
#     return await show_multiple_choice_controller(current_user)

# # execute the smart multiple choice
# @app.post("/smart_multiple_choice")
# async def smart_multiple_choice(question: MultipleChoiceAnswerFromUser, current_user: str = Depends(get_current_user)):
#     return await smart_multiple_choice_controller(current_user, question)


# listening smart question
@app.get("/show_listening_question", response_model=List[ListeningQuestion])
async def show_listening_question(level: str = Query(...),current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await show_listening_question_controller(level, current_user_id, db)
# When the user sends the answer, it will be same as the smart question. 
@app.post("/listening_question")
async def listeing_question(question: MultipleChoiceAnswerFromUserListening, current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await listening_question_controller(current_user_id, question, db)


# add question
@app.put("/add_questions")  # Note the change to 'add_questions' to reflect handling multiple questions
async def add_questions(questions_data: List[Dict] = Body(...), question_type: str = Query(..., description="The type of the questions to add ('smart' or 'listening')"), current_user: str = Depends(get_current_user), db=Depends(get_db)):
    if question_type not in ['smart', 'listening', 'grammar', 'writing']:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid question type")
    
    # Process list of questions
    results = []
    for question_data in questions_data:
        result = await add_question_controller(question_data, question_type, current_user, db)
        results.append(result)
    return results

# checking essay (grammar and meaning)
# using the question object in models
@app.post("/checking_essay")
async def checking_essay(user_answer: EssayAnswer, way_to_check = Query(...), current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await checking_essay_controller(user_answer, way_to_check, current_user_id, db)


# Modify the route handler to include dependency injection for the database
@app.get("/show_writing_question", response_model=List[EssayExam])
async def show_writing_question(level: str = Query(...), current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await show_writing_questions_controllers(level, current_user_id, db)

# grammar questions
@app.get("/show_grammar_questions", response_model=List[GrammarQuestion])
async def show_grammar_question(question_topic: str = Query(...),current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await show_grammar_question_controllers(question_topic, current_user_id, db)

@app.post("/grammar_questions")
async def process_grammar_question(user_answer: MultipleChoiceAnswerFromUser, current_user_id: int = Depends(get_current_user), db=Depends(get_db)):
    return await process_grammar_question_controller(user_answer, current_user_id, db)


# speaking
# create a thread id for speaking
@app.post("/create_thread_speaking")
async def create_thread_speaking(
    current_user_id: int = Depends(get_current_user), db=Depends(get_db)
):
    return await process_create_thread_controller(current_user_id, db)

# thread conversation
@app.post("/speaking")
async def speaking(thread_id: str = Query(...), audio_file: UploadFile = File(...)):
    return await speaking_controller(thread_id, audio_file)


# For debugging with breakpoints in VS code
if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=int(os.environ.get("PORT", 8000)), log_level="info")

